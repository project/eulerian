/**
 * @file
 * Attaches colorbox management.
 */

((document, $, drupalSettings) => {
  

  drupalSettings.eulerian = drupalSettings.eulerian || {};

  $(document).ready(() => {
    // Colorbox: This event triggers when the transition has completed and the
    // newly loaded content has been revealed.
    $(document).bind("cbox_complete", () => {
      // Check if Eulerian is initialized.
      if (typeof EA_push !== "function") {
        return;
      }

      const href = $.colorbox.element().attr("href");
      if (href) {
        let EA_datalayer = Object.assign({}, drupalSettings.eulerian.datalayer);
        EA_datalayer.path = href;

        // Remove "error" property in case of current page is in error.
        delete EA_datalayer.error;

        EA_push(EA_prepare2Push(EA_datalayer));
      }
    });
  });
})(document, jQuery, drupalSettings);
