/**
 * @file
 * Attaches several event listener to a web page.
 */

((document) => {


  /**
   The Eulerian event attribute suffix.
   *
   * @type {string}
   */
  const EA_ATTR_EVENT_TYPE = "event";
  const EA_ATTR_EVENT_NAME = "event-name";
  const EA_ATTR_PRODUCT_REF = "product-ref";

  /**
   * The Eulerian bind events.
   *
   * @type {string[]}
   */
  const EA_EVENT_BIND = ["keyup", "mousedown", "touchstart"];

  /**
   * The Eulerian event target.
   *
   * @type {string}
   */
  const EA_EVENT_TARGET = "a,area,button";

  /**
   * The Eulerian managed event types.
   *
   * @type {string[]}
   */
  const EA_EVENT_TYPES = ["action", "button", "download", "event", "link"];
  const EA_PRODUCT_EVENT_TYPES = ["productbutton", "productdownload", "productevent", "productlink"];

  /**
   * Default event binding.
   *
   * Attach mousedown, keyup, touchstart events to document only and catch
   * clicks on all elements.
   */
  const EA_defaultBind = () => {
    EA_EVENT_BIND.forEach((eventBind) => {
      document.body.addEventListener(eventBind,function (event) {
        // Check if Eulerian is initialized.
        if (typeof EA_push !== "function") {
          return;
        }

        // Catch the closest surrounding link of a clicked element.
        const closestElement = event.target.closest(EA_EVENT_TARGET);
        if (!closestElement) {
          return;
        }

        // Get Eulerian attributes defined into element.
        // You need to define event type to push event to Eulerian with module tracking feature.
        const EA_attributes = EA_getAttributes(closestElement);
        if (
          !EA_attributes ||
          typeof EA_attributes[EA_ATTR_EVENT_TYPE] === "undefined" ||
          (
            !EA_EVENT_TYPES.includes(EA_attributes[EA_ATTR_EVENT_TYPE]) &&
            !EA_PRODUCT_EVENT_TYPES.includes(EA_attributes[EA_ATTR_EVENT_TYPE])
          )
        ) {
          return;
        }

        const EA_eventType = EA_attributes[EA_ATTR_EVENT_TYPE];
        delete EA_attributes[EA_ATTR_EVENT_TYPE];

        // Allow user to choose Eulerian event name.
        let EA_eventName = event.target.innerText;
        if (
          typeof EA_attributes[EA_ATTR_EVENT_NAME] !== "undefined" &&
          EA_attributes[EA_ATTR_EVENT_NAME] !== ""
        ) {
          EA_eventName = EA_attributes[EA_ATTR_EVENT_NAME];
          delete EA_attributes[EA_ATTR_EVENT_NAME];
        }

        if (EA_eventType === "action") {
          // Push "action" event.
          EA_push(EA_eventType, EA_prepare2PushEvent(EA_attributes));
          return;
        }

        // Product events expect a reference.
        let EA_productRef = "";
        if (
            EA_PRODUCT_EVENT_TYPES.includes(EA_attributes[EA_ATTR_PRODUCT_REF]) &&
            typeof EA_attributes[EA_ATTR_PRODUCT_REF] === "undefined"
        ) {
          return;
        }
        else {
          EA_productRef = EA_attributes[EA_ATTR_PRODUCT_REF];
          delete EA_attributes[EA_ATTR_PRODUCT_REF];
        }

        if (Object.keys(EA_attributes).length > 0) {
          // Push custom parameters to associate them to event.
          EA_push("globalarg", EA_prepare2PushEvent(EA_attributes));
        }

        if (EA_productRef !== "") {
          // Push product event.
          EA_push(EA_eventType, [EA_productRef, EA_cleanString(EA_eventName)]);
        }
        else {
          // Push simple event.
          EA_push(EA_eventType, [EA_cleanString(EA_eventName)]);
        }

        if (Object.keys(EA_attributes).length > 0) {
          // Clear "globalarg" between two calls.
          EA_push("globalarg", []);
        }
      });
    });
  }

  document.addEventListener("DOMContentLoaded",() => {
    EA_defaultBind();
  });
})(document);
