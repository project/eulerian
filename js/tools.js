((document, window, Drupal, drupalSettings) => {


  drupalSettings.eulerian = drupalSettings.eulerian || {};

  /**
   * The Eulerian action event mandatory attributes.
   *
   * @see https://eulerian.wiki/doku.php?id=en:modules:collect:onsite_collection:eaactions
   *
   * @type {array}
   */
  const EA_ACTION_MANDATORY_ATTRIBUTES = ["actionname"];

  /**
   * These declaring action attributes needs to be placed in correct order if
   * defined.
   *
   * @type {array}
   */
  const EA_ACTION_DECLARING_ATTRIBUTES = ["actionmode", "actionlabel", "actionhref"];

  /**
   * The Eulerian element attribute prefix.
   *
   * @type {string}
   */
  const EA_ATTR_PREFIX = "data-eulerian-";

  /**
   * Clean string before to send it to Eulerian.
   *
   * @param {string} text
   *   The text to clean.
   *
   * @return {string}
   *   The cleaned text.
   */
  window.EA_cleanString = (text) => {
    // In case of formating is disabled, returns original string.
    if (!drupalSettings.eulerian.clean_string) {
      return text;
    }

    // Remove HTML tags and entities.
    const element = document.createElement("div");
    element.innerHTML = text;
    text = element.textContent || element.innerText || "";

    // Remove accented characters.
    text = text.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

    // Replace spaces with underscore.
    text = text.replace(/\s+/g, "_");

    return text.trim().toLowerCase();
  };

  /**
   * Get Eulerian attributes.
   *
   * @param {Element} element
   *   The given element object.
   *
   * @return {Object}
   *   The Eulerian attributes.
   */
  window.EA_getAttributes = (element) => {
    const attributeNames = element.getAttributeNames();
    const regex = new RegExp("^" + EA_ATTR_PREFIX, "i");
    let EA_attributes = {};

    attributeNames.forEach((name) => {
      if (regex.test(name)) {
        EA_attributes[name.substring(EA_ATTR_PREFIX.length)] = EA_cleanString(element.getAttribute(name));
      }
    });

    return EA_attributes;
  };

  /**
   * Convert associative array to Eulerian datalayer (before to push it).
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The Eulerian attributes.
   */
  window.EA_prepare2Push = (data) => {
    let dataClone = Object.assign({}, data);

    // Manage specific use cases.
    let EA_data = _prepare2PushProducts(dataClone).concat(
      _prepare2PushProductReferences(dataClone),
      _prepare2PushSearchKeys(dataClone)
    );

    for (const [key, value] of Object.entries(dataClone)) {
      // Keys are assumed to be correctly formatted and can be used as is.
      EA_data.push(key, value);
    }

    return EA_data;
  }

  /**
   * Convert associative array to Eulerian datalayer (before to push event).
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The Eulerian attributes.
   */
  window.EA_prepare2PushEvent = (data) => {
    let dataClone = Object.assign({}, data);

    // Manage specific use cases for "action" event type.
    let EA_data = _prepare2PushAction(dataClone);

    for (const [key, value] of Object.entries(dataClone)) {
      EA_data.push(key, EA_cleanString(value));
    }

    return EA_data;
  }

  /**
   * Define action valies as first array's element.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The action attributes to push in first.
   */
  const _prepare2PushActionMandatoryValues = (data) => {
    let EA_data = [];

    EA_ACTION_MANDATORY_ATTRIBUTES.forEach((attributeName) => {
      // Push all mandatory values or any.
      if (!data.hasOwnProperty(attributeName)) {
        return [];
      }

      // Action event needs to have mandatory attributes defined in first.
      EA_data.push(attributeName, data[attributeName]);

      delete data[attributeName];
    });

    return EA_data;
  };

  /**
   * Action declaring attributes.
   * If they are defined, they need places in correct order.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The action names to push.
   */
  const _prepare2PushActionDeclaringAttributes = (data) => {
    let EA_data = [];

    EA_ACTION_DECLARING_ATTRIBUTES.forEach((attributeName) => {
      // Push all declaring attributes' value or any.
      if (!data.hasOwnProperty(attributeName)) {
        return [];
      }

      // Action event needs declaring attributes defined after mandatory ones.
      EA_data.push(attributeName, data[attributeName]);

      delete data[attributeName];
    });

    return EA_data;
  }

  /**
   * Manage specif use cases for "action" event type.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The action names to push.
   */
  const _prepare2PushAction = (data) => {
    _validateActionMandatoryAttributes(data);

    return _prepare2PushActionMandatoryValues(data).concat(
      _prepare2PushActionDeclaringAttributes(data),
      _prepare2PushActionParameters(data),
      _prepare2PushActions(data),
    );
  };

  /**
   * Give possibility to define a collection of actions.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The action names to push.
   */
  const _prepare2PushActions = (data) => {
    let EA_data = [];

    if (
      !data.hasOwnProperty("actions") ||
      typeof data.actions !== "object"
    ) {
      return EA_data;
    }

    Object.values(data.actions).forEach((item) => {
      if (typeof item === "object") {
        EA_data = EA_data.concat(
          _prepare2PushAction(item)
        );

        for (const [key, value] of Object.entries(item)) {
          EA_data.push(key, value);
        }
      }
    });

    delete data.actions;

    return EA_data;
  };

  /**
   * Give possibility to define a collection of action parameters name and value.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The action names to push.
   */
  const _prepare2PushActionParameters = (data) => {
    let EA_data = [];

    if (
      !data.hasOwnProperty("actionparams") ||
      typeof data.actionparams !== "object"
    ) {
      return EA_data;
    }

    Object.values(data.actionparams).forEach((item) => {
      if (
        typeof item === "object" &&
        item.hasOwnProperty("name") &&
        item.hasOwnProperty("value")
      ) {
        EA_data.push("actionpname", item.name, "actionpvalue", item.value);

        if (item.hasOwnProperty("finite")) {
          EA_data.push("actionpfinite", item.finite);
        }
      }
    });

    delete data.actionparams;

    return EA_data;
  };

  /**
   * Give possibility to define a collection of products.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The products to push.
   */
  const _prepare2PushProducts = (data) => {
    let EA_data = [];

    if (
      !data.hasOwnProperty("products") ||
      typeof data.products !== "object"
    ) {
      return EA_data;
    }

    Object.values(data.products).forEach((item) => {
      if (
        typeof item === "object" &&
        item.hasOwnProperty("ref") &&
        item.hasOwnProperty("amount") &&
        item.hasOwnProperty("quantity")
      ) {
        EA_data.push("prdref", item.ref, "prdamount", item.amount, "prdquantity", item.quantity);
      }
    });

    delete data.products;

    return EA_data;
  };

  /**
   * Give possibility to define a collection of product references.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The product references to push.
   */
  const _prepare2PushProductReferences = (data) => {
    let EA_data = [];

    if (
      !data.hasOwnProperty("prdrefs") ||
      !Array.isArray(data.prdrefs)
    ) {
      return EA_data;
    }

    data.prdrefs.forEach((value) => {
      EA_data.push("prdref", value);
    });

    delete data.prdrefs;

    return EA_data;
  };

  /**
   * Give possibility to define a collection of search keys and data.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @return {Object}
   *   The search keys to push.
   */
  const _prepare2PushSearchKeys = (data) => {
    let EA_data = [];

    if (
      !data.hasOwnProperty("isearchkeys") ||
      typeof data.isearchkeys !== "object"
    ) {
      return EA_data;
    }

    for (const [key, value] of Object.entries(data.isearchkeys)) {
      EA_data.push("isearchkey", key, "isearchdata", value);
    }

    delete data.isearchkeys;

    return EA_data;
  };

  /**
   * Validate action event mandatory attributes.
   *
   * @param {Object} data
   *   The data object to push.
   *
   * @throws {string}
   *   Throws error if mandatory attributes are not defined.
   */
  const _validateActionMandatoryAttributes = (data) => {
    let countAttributes = 0;
    EA_ACTION_MANDATORY_ATTRIBUTES.forEach((attributeName) => {
      if (data.hasOwnProperty(attributeName)) {
        countAttributes += 1;
      }
    });

    if (countAttributes > 0 && countAttributes !== EA_ACTION_MANDATORY_ATTRIBUTES.length) {
      throw Drupal.t("All mandatory action attributes are not defined. You must declare attributes: @attributes.", {"@attributes": EA_ACTION_MANDATORY_ATTRIBUTES.join(", ")});
    }
  };

})(document, window, Drupal, drupalSettings);
