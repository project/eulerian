<?php

declare(strict_types=1);

namespace Drupal\eulerian_commerce_cart\Services;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides helper functions.
 */
class CommerceCartHelper implements CommerceCartHelperInterface {

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected CartProviderInterface $cartProvider;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * CommerceCartHelper constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The RouteMatch service.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   */
  public function __construct(
    RouteMatchInterface $route_match,
    CartProviderInterface $cart_provider
  ) {
    $this->routeMatch = $route_match;
    $this->cartProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function supplyDatalayer(): array {
    $datalayer = [];

    if ($this->routeMatch->getRouteName() === 'commerce_cart.page') {
      return $this->supplyCartDatalayer() + $datalayer;
    }

    return $datalayer;
  }

  /**
   * Supply "Cart" page datalayer.
   *
   * @return array
   *   The supplied "Cart" datalayer.
   */
  private function supplyCartDatalayer(): array {
    $datalayer = [
      'scart' => 1,
      // Value: 0 (= entire cart), 1 (= products accumulation).
      'scartcumul' => 0,
      'products' => [],
    ];

    foreach ($this->cartProvider->getCarts() as &$cart) {
      foreach ($cart->getItems() as $item) {
        $productVariation = $item->getPurchasedEntity();
        if (!$productVariation instanceof PurchasableEntityInterface) {
          continue;
        }

        $product = $productVariation->getProduct();
        if (!$product instanceof ProductInterface) {
          continue;
        }

        $datalayer['products'][] = [
          'ref' => $product->uuid(),
          'amount' => $productVariation->getPrice()->getNumber(),
          'quantity' => $item->getQuantity(),
        ];
      }
    }

    return $datalayer;
  }

}
