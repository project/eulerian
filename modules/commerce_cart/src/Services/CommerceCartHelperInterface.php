<?php

declare(strict_types=1);

namespace Drupal\eulerian_commerce_cart\Services;

/**
 * Provides a helper interface.
 */
interface CommerceCartHelperInterface {

  /**
   * Supply datalayer with current page context.
   *
   * @return array
   *   The datalayer supplied.
   */
  public function supplyDatalayer(): array;

}
