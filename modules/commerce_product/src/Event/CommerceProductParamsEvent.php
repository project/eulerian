<?php

namespace Drupal\eulerian_commerce_product\Event;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Cache\CacheableResponseTrait;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event fired when a commerce product datalayer is being built.
 *
 * Subscribers to this event should supply "prdparam" datalayer attributes.
 */
class CommerceProductParamsEvent extends Event {

  use CacheableResponseTrait;

  /**
   * The "Product" entity object.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected ProductInterface $product;


  /**
   * An associative product parameters array to supply to datalayer.
   *
   * @var array
   */
  protected array $parameters = [];

  /**
   * Creates a new EulerianCommercePrdParamEvent object.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The commerce product entity object.
   */
  public function __construct(ProductInterface $product) {
    $this->product = $product;
  }

  /**
   * Get the commerce product entity object.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   *   The commerce product entity object.
   */
  public function getProduct(): ProductInterface {
    return $this->product;
  }

  /**
   * Get the product parameters in its current state.
   *
   * @return array
   *   The product parameters array built by the event subscribers.
   */
  public function getParameters(): array {
    return $this->parameters;
  }

  /**
   * Get the parameter value for given key.
   *
   * @param string $key
   *   The key to set.
   *
   * @return mixed
   *   The value if it exists. NULL, otherwise.
   */
  public function getParameter(string $key): mixed {
    return $this->parameters[$key] ?? NULL;
  }

  /**
   * Sets the value for a specific key.
   *
   * @param string $key
   *   The key to set.
   * @param mixed $value
   *   The value to set.
   *
   * @return $this
   */
  public function setParameter(string $key, $value): CommerceProductParamsEvent {
    $this->parameters[$key] = $value;
    return $this;
  }

  /**
   * Set the product parameters.
   *
   * @param array $parameters
   *   A product parameters array to supply to datalayer.
   *
   * @return $this
   */
  public function setParameters(array $parameters): CommerceProductParamsEvent {
    $this->parameters = $parameters;
    return $this;
  }

}
