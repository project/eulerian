<?php

declare(strict_types=1);

namespace Drupal\eulerian_commerce_product\Services;

/**
 * Provides a helper interface.
 */
interface CommerceProductHelperInterface {

  /**
   * Supply datalayer with current page context.
   *
   * @return array
   *   The datalayer supplied.
   */
  public function supplyDatalayer(): array;

}
