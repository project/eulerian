<?php

declare(strict_types=1);

namespace Drupal\eulerian_commerce_product\Services;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\eulerian_commerce_product\Event\CommerceProductParamsEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides helper functions.
 */
class CommerceProductHelper implements CommerceProductHelperInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * CommerceProductHelper constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The RouteMatch service.
   */
  public function __construct(
    EventDispatcherInterface $event_dispatcher,
    RequestStack $request_stack,
    RouteMatchInterface $route_match
  ) {
    $this->eventDispatcher = $event_dispatcher;
    $this->request = $request_stack->getCurrentRequest();
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function supplyDatalayer(): array {
    $datalayer = [];

    $product = $this->request->attributes->get('commerce_product');
    if (
      $product instanceof ProductInterface &&
      $this->routeMatch->getRouteName() === 'entity.commerce_product.canonical'
    ) {
      return $this->supplyProductDatalayer($product) + $datalayer;
    }

    return $datalayer;
  }

  /**
   * Supply "Product" page datalayer.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The given commerce_product object.
   *
   * @return array
   *   The supplied "Product" datalayer.
   */
  private function supplyProductDatalayer(ProductInterface $product): array {
    $datalayer = [
      'prdref' => $product->uuid(),
      'prdname' => $product->label(),
    ];

    // Allow users to supply datalayer with product parameters.
    $event = new CommerceProductParamsEvent($product);
    $this->eventDispatcher->dispatch($event);

    $parameters = $event->getParameters();
    foreach ($parameters as $name => $value) {
      $datalayer['prdparam-' . $name] = (string) $value;
    }

    return $datalayer;
  }

}
