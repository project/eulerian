<?php

declare(strict_types=1);

namespace Drupal\eulerian_commerce_checkout\Services;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides helper functions.
 */
class CommerceCheckoutHelper implements CommerceCheckoutHelperInterface {

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected CheckoutOrderManagerInterface $checkoutOrderManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * CommerceCheckoutHelper constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The RouteMatch service.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   */
  public function __construct(
    RouteMatchInterface $route_match,
    CheckoutOrderManagerInterface $checkout_order_manager
  ) {
    $this->routeMatch = $route_match;
    $this->checkoutOrderManager = $checkout_order_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function supplyDatalayer(): array {
    $datalayer = [];

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->routeMatch->getParameter('commerce_order');
    if ($order instanceof OrderInterface) {
      $requested_step_id = $this->routeMatch->getParameter('step');
      $step_id = $this->checkoutOrderManager->getCheckoutStepId($order, $requested_step_id);

      if (
        $requested_step_id === $step_id &&
        $requested_step_id === 'complete'
      ) {
        return $this->supplyCheckoutCompletedDatalayer($order) + $datalayer;
      }
    }

    return $datalayer;
  }

  /**
   * Supply "Checkout completed" page datalayer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The given commerce_order object.
   *
   * @return array
   *   The supplied "Checkout completed" datalayer.
   */
  private function supplyCheckoutCompletedDatalayer(OrderInterface $order): array {
    $datalayer = [
      'ref' => $order->uuid(),
      'amount' => $order->getTotalPrice()->getNumber(),
      'currency' => $order->getTotalPrice()->getCurrencyCode(),
      'products' => [],
    ];

    foreach ($order->getItems() as $item) {
      $productVariation = $item->getPurchasedEntity();
      if (!$productVariation instanceof PurchasableEntityInterface) {
        continue;
      }

      $product = $productVariation->getProduct();
      if (!$product instanceof ProductInterface) {
        continue;
      }

      $datalayer['products'][] = [
        'ref' => $product->uuid(),
        'amount' => $productVariation->getPrice()->getNumber(),
        'quantity' => $item->getQuantity(),
      ];
    }

    return $datalayer;
  }

}
