<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\eulerian\EulerianInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test Eulerian helper of Eulerian module.
 *
 * @group eulerian
 */
class EulerianVisibilityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
    'path_alias',
    'php',
    'system',
  ];

  /**
   * Do the initial setup.
   */
  public function setup(): void {
    parent::setUp();

    $this->installConfig(['eulerian', 'system']);

    // Provide a mock service container,
    // for the service "cache_tags.invalidator".
    $cacheTagsInvalidator = $this->createMock('Drupal\Core\Cache\CacheTagsInvalidatorInterface');

    $container = new ContainerBuilder();
    $container->set('cache_tags.invalidator', $cacheTagsInvalidator);
    \Drupal::setContainer($container);
  }

  /**
   * Test enabled pages.
   *
   * @dataProvider enabledOnCurrentPageProvider
   */
  public function testIsEnabledOnCurrentPage(string $mode, string $pages, bool $expectedReturn): void {
    $this->config('eulerian.settings')
      ->set('visibility.request_path_mode', $mode)
      ->set('visibility.request_path_pages', $pages)
      ->save();

    $this->assertEquals(
      $expectedReturn,
      $this->container->get('eulerian.visibility')->isEnabledOnCurrentPage()
    );
  }

  /**
   * Provides test data for ::testIsEnabledOnCurrentPage().
   *
   * @return array
   *   An array of test cases, each test case is an array with three values:
   *   0. A string containing the track mode to use.
   *   0. A string containing a list of pages to track.
   *   1. A boolean indicating whether or not a validation error is expected to
   *      be thrown.
   */
  public function enabledOnCurrentPageProvider(): array {
    return [
      [
        EulerianInterface::TRACKING_REQUEST_MODE_ALL,
        '',
        TRUE,
      ],
      [
        EulerianInterface::TRACKING_REQUEST_MODE_ALL,
        '/admin',
        TRUE,
      ],
      [
        EulerianInterface::TRACKING_REQUEST_MODE_LISTED,
        '/admin',
        FALSE,
      ],
      [
        EulerianInterface::TRACKING_REQUEST_MODE_PHP,
        '<?php return TRUE; ?>',
        TRUE,
      ],
      [
        EulerianInterface::TRACKING_REQUEST_MODE_PHP,
        '<?php return FALSE; ?>',
        FALSE,
      ],
    ];
  }

}
