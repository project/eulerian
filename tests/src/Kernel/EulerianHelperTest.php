<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\Kernel;

use Drupal\Core\Site\Settings;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test Eulerian helper of Eulerian module.
 *
 * @group eulerian
 */
class EulerianHelperTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
  ];

  /**
   * The mocked hash salt.
   *
   * @var string
   */
  const HAST_SALT = 'mock hash salt';

  /**
   * Do the initial setup.
   */
  public function setup(): void {
    parent::setUp();

    $settings = Settings::getAll();
    $settings['hash_salt'] = static::HAST_SALT;
    new Settings($settings);

    $this->installConfig(['eulerian', 'system']);
  }

  /**
   * Test string cleaning.
   */
  public function testCleanString(): void {
    $this->assertSame(
      'aaaaaaaaaa_cc_eeeeeeee_iiiiiiii_oooooooooo_uuuuuuuu_yy',
      $this->container->get('eulerian.helper')
        ->cleanString('ÀàÁáÂâÃãÄä Çç ÈèÉéÊêËë ÌìÍíÎîÏï ÒòÓóÔôÕõÖö ÙùÚúÛûÜü Ýÿ -_,.')
    );
  }

  /**
   * Test user identifier hash generation.
   */
  public function testGenerateUserIdentifierHash(): void {
    $this->container->get('state')
      ->set('system.private_key', 'abcefghijklmnopqrstuvwxyz0123456789');

    // Check that the identifier is fixed for the same user ID.
    $this->assertSame(
      $this->container->get('eulerian.helper')->generateUserIdentifierHash(123),
      $this->container->get('eulerian.helper')->generateUserIdentifierHash(123)
    );

    // Check that the identifier hash is specific to the user ID.
    $this->assertNotSame(
      $this->container->get('eulerian.helper')->generateUserIdentifierHash(123),
      $this->container->get('eulerian.helper')->generateUserIdentifierHash(456)
    );
  }

}
