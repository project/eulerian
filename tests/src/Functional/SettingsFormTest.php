<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\Functional;

use Drupal\eulerian\EulerianInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests the configuration form.
 *
 * @group eulerian
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A test administrator.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $permissions = [
      'access administration pages',
      'administer eulerian',
    ];

    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the fields that allow to configure tracking of specific pages.
   *
   * @param string $pages
   *   A list of pages that should be tracked. One page per line.
   * @param bool $validation_error_expected
   *   TRUE if a validation error should be thrown. FALSE otherwise.
   *
   * @dataProvider pageTrackingProvider
   */
  public function testPageTracking(string $pages, bool $validation_error_expected): void {
    $edit = [
      'visibility[request_path_mode]' => EulerianInterface::TRACKING_REQUEST_MODE_ALL,
      'visibility[request_path_pages]' => $pages,
    ];
    $this->drupalGet('admin/config/system/eulerian');
    $this->submitForm($edit, 'Save configuration');
    $hasValidationError = (bool) $this->getSession()->getPage()->find('css', '#edit-visibility-request-path-pages.error');
    $this->assertEquals($validation_error_expected, $hasValidationError);
  }

  /**
   * Provides test data for ::testPageTracking().
   *
   * @return array
   *   An array of test cases, each test case is an array with two values:
   *   0. A string containing a list of pages to track.
   *   1. A boolean indicating whether or not a validation error is expected to
   *      be thrown.
   */
  public function pageTrackingProvider(): array {
    // @codingStandardsIgnoreStart
    return [
      [
        // No validation error should be thrown for an empty page list.
        '',
        FALSE,
      ],
      [
        // No validation error should be thrown for a list of valid pages.
        <<<'TXT'
/node/1
/blog/*/view
/admin
<front>
TXT
        ,
        FALSE,
      ],
      [
        // A validation error should be thrown if one of the pages doesn't start
        // with a slash.
        <<<'TXT'
/node/1
/blog/*/view
admin
<front>
TXT
        ,
        TRUE,
      ],
    ];
    // @codingStandardsIgnoreEnd
  }

}
