<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\Functional;

use Drupal\eulerian\EulerianInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Test basic functionality of Eulerian module.
 *
 * @group eulerian
 */
class EulerianBasicTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $permissions = [
      'access administration pages',
      'administer eulerian',
    ];

    // User to set up eulerian.
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if configuration is possible.
   */
  public function testEulerianConfiguration(): void {
    // Check for setting page's presence.
    $this->drupalGet('admin/config/system/eulerian');
    $this->assertSession()->responseContains('Domain');
  }

  /**
   * Tests if page visibility works.
   */
  public function testEulerianPageVisibility(): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->save();

    // Show tracking on "every page except the listed pages".
    $this->config('eulerian.settings')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->set('visibility.request_path_pages', '')
      ->save();

    // Check tracking code visibility.
    $this->drupalGet('');
    $this->assertSession()->responseContains('/eulerian/js/events.js');
    $this->assertSession()->responseContains('/eulerian/js/init.js');
    $this->assertSession()->responseContains('/eulerian/js/tools.js');

    $this->drupalGet('admin');
    $this->assertSession()->responseContains('/eulerian/js/events.js');
    $this->assertSession()->responseContains('/eulerian/js/init.js');
    $this->assertSession()->responseContains('/eulerian/js/tools.js');

    $this->drupalGet('admin/config/system/eulerian');
    $this->assertSession()->responseContains('/eulerian/js/events.js');
    $this->assertSession()->responseContains('/eulerian/js/init.js');
    $this->assertSession()->responseContains('/eulerian/js/tools.js');

    // Disable tracking on "admin*" pages only.
    $this->config('eulerian.settings')
      ->set('visibility.request_path_pages', "/admin\n/admin/*")
      ->save();

    // Check tracking code visibility.
    $this->drupalGet('');
    $this->assertSession()->responseContains('/eulerian/js/events.js');
    $this->assertSession()->responseContains('/eulerian/js/init.js');
    $this->assertSession()->responseContains('/eulerian/js/tools.js');

    $this->drupalGet('admin');
    $this->assertSession()->responseNotContains('/eulerian/js/events.js');
    $this->assertSession()->responseNotContains('/eulerian/js/init.js');
    $this->assertSession()->responseNotContains('/eulerian/js/tools.js');

    $this->drupalGet('admin/config/system/eulerian');
    $this->assertSession()->responseNotContains('/eulerian/js/events.js');
    $this->assertSession()->responseNotContains('/eulerian/js/init.js');
    $this->assertSession()->responseNotContains('/eulerian/js/tools.js');

    // Show tracking on "every page except the listed pages".
    $this->config('eulerian.settings')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_LISTED)
      ->save();

    // Check tracking code visibility.
    $this->drupalGet('');
    $this->assertSession()->responseNotContains('/eulerian/js/events.js');
    $this->assertSession()->responseNotContains('/eulerian/js/init.js');
    $this->assertSession()->responseNotContains('/eulerian/js/tools.js');

    $this->drupalGet('admin');
    $this->assertSession()->responseContains('/eulerian/js/events.js');
    $this->assertSession()->responseContains('/eulerian/js/init.js');
    $this->assertSession()->responseContains('/eulerian/js/tools.js');

    $this->drupalGet('admin/config/system/eulerian');
    $this->assertSession()->responseContains('/eulerian/js/events.js');
    $this->assertSession()->responseContains('/eulerian/js/init.js');
    $this->assertSession()->responseContains('/eulerian/js/tools.js');
  }

}
