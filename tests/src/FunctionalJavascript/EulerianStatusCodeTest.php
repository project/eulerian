<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\FunctionalJavascript;

use Drupal\eulerian\EulerianInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test status code settings of Eulerian module.
 *
 * @group eulerian
 */
class EulerianStatusCodeTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests if error code is properly added to the error page.
   *
   * @dataProvider pageStatusCodeProvider
   */
  public function testEulerianErrorCode(string $path, string $response_expected): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->save();

    // Test whether 403 forbidden tracking code is shown if user has no access.
    $this->drupalGet($path);
    $this->assertSession()->responseContains($response_expected);

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $datalayer = &$jsSettings['eulerian']['datalayer'];

    $this->assertArrayHasKey('error', $datalayer);
    $this->assertSame($datalayer['error'], 1);
  }

  /**
   * Tests if error code is properly added to the error page.
   *
   * @dataProvider pageStatusCodeProvider
   */
  public function testEulerianWithoutCode(string $path, string $response_expected): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->set('status_codes_disabled', [403, 404])
      ->save();

    // Test whether 403 forbidden tracking code is shown if user has no access.
    $this->drupalGet($path);
    $this->assertSession()->responseContains($response_expected);

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $this->assertArrayNotHasKey('eulerian', $jsSettings);
  }

  /**
   * Provides test data for ::testEulerianStatusCode().
   *
   * And Provides test data for ::testEulerianWithoutCode().
   *
   * @return array
   *   An array of test cases, each test case is an array with two values:
   *   0. A string containing the page path to load.
   *   1. The expected page response.
   */
  public function pageStatusCodeProvider(): array {
    return [
      [
        // Check 403 status code.
        'admin',
        '"403/URL = "',
      ],
      [
        // Check 404 status code.
        $this->randomMachineName(64),
        '"404/URL = "',
      ],
    ];
  }

}
