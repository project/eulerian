<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\FunctionalJavascript;

use Drupal\Component\Utility\Html;
use Drupal\eulerian\EulerianInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\UserInterface;

/**
 * Test PHP filter functionnality of Eulerian module.
 *
 * @group eulerian
 */
class EulerianPhpFilterTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
    'php',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * Delegated admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $delegatedAdminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Administrator with all permissions.
    $permissionsAdminUser = [
      'access administration pages',
      'administer eulerian',
      'use php for eulerian tracking visibility',
    ];
    $this->adminUser = $this->drupalCreateUser($permissionsAdminUser);

    // Administrator who cannot configure tracking visibility with PHP.
    $permissionsDelegatedAdminUser = [
      'access administration pages',
      'administer eulerian',
    ];
    $this->delegatedAdminUser = $this->drupalCreateUser($permissionsDelegatedAdminUser);
  }

  /**
   * Tests if PHP module integration works.
   */
  public function testEulerianPhpFilter(): void {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/config/system/eulerian');

    $edit = [];
    $edit['tracking[domain]'] = 'xxx.example.com';
    // Skip url check errors in automated tests.
    $edit['visibility[request_path_mode]'] = 2;
    $edit['visibility[request_path_pages]'] = '<?php return 0; ?>';
    $this->submitForm($edit, 'Save configuration');

    // Compare saved setting with posted setting.
    $this->assertEquals(
      '<?php return 0; ?>',
      \Drupal::config('eulerian.settings')->get('visibility.request_path_pages'),
      '[testEulerianPhpFilter]: PHP code snippet is intact.'
    );

    // Check tracking code visibility.
    $this->config('eulerian.settings')->set('visibility.request_path_pages', '<?php return TRUE; ?>')->save();
    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    // Check JS settings have Eulerian settings.
    $this->assertArrayHasKey('eulerian', $jsSettings);

    $this->drupalGet('admin');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    // Check JS settings have Eulerian settings.
    $this->assertArrayHasKey('eulerian', $jsSettings);

    $this->config('eulerian.settings')->set('visibility.request_path_pages', '<?php return FALSE; ?>')->save();
    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    // Check JS settings n't Eulerian settings.
    $this->assertArrayNotHasKey('eulerian', $jsSettings);

    // Test administration form.
    $this->config('eulerian.settings')->set('visibility.request_path_pages', '<?php return TRUE; ?>')->save();
    $this->drupalGet('admin/config/system/eulerian');
    $this->assertSession()->responseContains('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
    $this->assertSession()->responseContains(Html::escape('<?php return TRUE; ?>'));

    // Login the delegated user and check if fields are visible.
    $this->drupalLogin($this->delegatedAdminUser);
    $this->drupalGet('admin/config/system/eulerian');
    $this->assertSession()->responseNotContains('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
    $this->assertSession()->responseNotContains(Html::escape('<?php return TRUE; ?>'));

    // Set a different value
    // and verify that this is still the same after the post.
    $this->config('eulerian.settings')->set('visibility.request_path_pages', '<?php return 0; ?>')->save();

    $this->drupalGet('admin/config/system/eulerian');

    $edit = [];
    $edit['tracking[domain]'] = 'xxx.example.com';
    $this->submitForm($edit, 'Save configuration');

    // Compare saved setting with posted setting.
    $this->assertEquals(
      EulerianInterface::TRACKING_REQUEST_MODE_PHP,
      $this->config('eulerian.settings')->get('visibility.request_path_mode'),
      '[testEulerianPhpFilter]: Pages on which this PHP code returns TRUE is selected.'
    );
    $this->assertEquals(
      '<?php return 0; ?>',
      $this->config('eulerian.settings')->get('visibility.request_path_pages'),
      '[testEulerianPhpFilter]: PHP code snippet is intact.'
    );
  }

}
