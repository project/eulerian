<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\FunctionalJavascript;

use Drupal\eulerian\EulerianInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test tracking domain settings of Eulerian module.
 *
 * @group eulerian
 */
class EulerianTrackingDomainTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests if tracking domain is properly added to the page.
   */
  public function testEulerianDomainTracking(): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->save();
    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    // Check JS settings have Eulerian domain.
    $this->assertArrayHasKey('eulerian', $jsSettings);
    $this->assertArrayHasKey('domain', $jsSettings['eulerian']);
    $this->assertSame('xxx.example.com', $jsSettings['eulerian']['domain']);
  }

  /**
   * Tests if tracking domain is not added to the page when is not set.
   */
  public function testEulerianWithoutDomainTracking(): void {
    $this->config('eulerian.settings')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->save();
    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    // Check JS settings haven't Eulerian settings.
    $this->assertArrayNotHasKey('eulerian', $jsSettings);
  }

}
