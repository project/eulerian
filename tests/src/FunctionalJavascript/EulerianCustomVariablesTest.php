<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\FunctionalJavascript;

use Drupal\eulerian\EulerianInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test custom variables functionality of Eulerian module.
 *
 * @group eulerian
 */
class EulerianCustomVariablesTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
    'token',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests if custom variables are properly added to the page.
   *
   * @dataProvider customEmptyVariablesProvider
   */
  public function testEulerianCustomVariables(string $name, string $value): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->set('custom.parameters', [['name' => $name, 'value' => $value]])
      ->save();

    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $datalayer = &$jsSettings['eulerian']['datalayer'];

    $this->assertArrayHasKey($name, $datalayer);
    $this->assertSame($value, $datalayer[$name]);
  }

  /**
   * Tests custom variables with empty name or value.
   *
   * These are not added into datalayer.
   *
   * @dataProvider customEmptyVariablesProvider
   */
  public function testEulerianCustomEmptyVariables(string $name, string $value): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->set('custom.parameters', [['name' => $name, 'value' => $value]])
      ->save();

    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $this->assertArrayNotHasKey($name, $jsSettings['eulerian']['datalayer']);
  }

  /**
   * Tests if custom variables with tokens are properly added to the page.
   */
  public function testEulerianCustomVariablesWithTokens(): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->set('custom.parameters', [
        [
          'name' => 'name_[site:slogan]',
          'value' => 'Value: [site:slogan]',
        ],
      ])
      ->save();

    // Test whether tokens are replaced in custom variable names.
    $site_slogan = $this->randomMachineName(16);
    $this->config('system.site')->set('slogan', $site_slogan)->save();

    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $datalayer = &$jsSettings['eulerian']['datalayer'];

    $this->assertArrayHasKey('name_' . $site_slogan, $datalayer);
    $this->assertSame('Value: ' . $site_slogan, $datalayer['name_' . $site_slogan]);
  }

  /**
   * Provides test data for ::testEulerianCustomEmptyVariables().
   *
   * @return array
   *   An array of test cases, each test case is an array with two values:
   *   0. A string containing the custom variable name.
   *   1. A string containing the custom variable value.
   */
  public function customEmptyVariablesProvider(): array {
    return [
      [
        '',
        $this->randomMachineName(16),
      ],
      [
        $this->randomMachineName(16),
        '',
      ],
      [
        '',
        '',
      ],
    ];
  }

  /**
   * Provides test data for ::testEulerianCustomVariables().
   *
   * @return array
   *   An array of test cases, each test case is an array with two values:
   *   0. A string containing the custom variable name.
   *   1. A string containing the custom variable value.
   */
  public function customVariablesProvider(): array {
    return [
      [
        'foo1',
        'Bar 1',
      ],
      [
        'foo2',
        'Bar 2',
      ],
      [
        'foo3',
        'Bar 3',
      ],
    ];
  }

}
