<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\FunctionalJavascript;

use Drupal\eulerian\EulerianInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\UserInterface;

/**
 * Test tracking user settings of Eulerian module.
 *
 * @group eulerian
 */
class EulerianTrackingUserTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
    'pathauto',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // User to set up eulerian.
    $this->user = $this->drupalCreateUser();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests if "uid" tracking code is properly added to the page.
   */
  public function testEulerianUidTrackingCode(): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('track.userid', TRUE)
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->save();
    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $datalayer = &$jsSettings['eulerian']['datalayer'];

    $this->assertArrayHasKey('uid', $datalayer);
    $this->assertNotEmpty('uid', $datalayer);
  }

  /**
   * Tests if "uid" tracking code is not added to the page.
   */
  public function testEulerianWithoutUidTrackingCode(): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('track.userid', FALSE)
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->save();
    $this->drupalGet('');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $datalayer = &$jsSettings['eulerian']['datalayer'];

    $this->assertArrayNotHasKey('uid', $datalayer);
  }

}
