<?php

declare(strict_types=1);

namespace Drupal\Tests\eulerian\FunctionalJavascript;

use Drupal\eulerian\EulerianInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\UserInterface;

/**
 * Test search functionnality of Eulerian module.
 *
 * @group eulerian
 */
class EulerianSearchTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'eulerian',
    'node',
    'search',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    $permissions = [
      'access administration pages',
      'administer eulerian',
      'search content',
      'create page content',
      'edit own page content',
    ];

    // User to set up eulerian.
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if search tracking is properly added to the page.
   */
  public function testEulerianSearchTracking(): void {
    $this->config('eulerian.settings')
      ->set('track.domain', 'xxx.example.com')
      ->set('visibility.request_path_mode', EulerianInterface::TRACKING_REQUEST_MODE_ALL)
      ->save();

    // Check tracking code visibility.
    $this->drupalGet('search/node');

    // Check that we can read the JS settings.
    $jsSettings = $this->getDrupalSettings();

    $datalayer = &$jsSettings['eulerian']['datalayer'];

    $this->assertArrayNotHasKey('isearchengine', $datalayer);
    $this->assertArrayNotHasKey('isearchkey', $datalayer);
    $this->assertArrayNotHasKey('isearchdata', $datalayer);
    $this->assertArrayNotHasKey('isearchresults', $datalayer);

    // Enable site search support.
    $this->config('eulerian.settings')->set('track.site_search', 1)->save();

    // Search for random string.
    $search = [];
    $search['keys'] = $this->randomMachineName(8);

    // Search on page without results.
    $this->drupalGet('search/node');

    // Fire a search, it's expected to get 0 results.
    $this->submitForm($search, 'Search');

    $this->assertArrayHasKey('isearchengine', $datalayer);
    $this->asertSame('Search', $datalayer['isearchengine']);
    $this->assertArrayHasKey('isearchkey', $datalayer);
    $this->asertSame('keys', $datalayer['isearchkey']);
    $this->assertArrayHasKey('isearchdata', $datalayer);
    $this->asertSame($search['keys'], $datalayer['isearchdata']);
    $this->assertArrayHasKey('isearchresults', $datalayer);
    $this->asertSame(0, $datalayer['isearchresults']);

    // Create a node to search for.
    $this->drupalGet('node/add/page');

    $edit = [];
    $edit['title[0][value]'] = 'This is a test title';
    $edit['body[0][value]'] = 'This test content contains ' . $search['keys'] . ' string.';

    // Save the node.
    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains($this->t('@type @title has been created.', [
      '@type' => 'Basic page',
      '@title' => $edit['title[0][value]'],
    ]));

    // Index the node or it cannot found.
    $this->cronRun();
    $this->drupalGet('search/node');

    $this->submitForm($search, 'Search');

    // Check 1 result was found.
    $this->asertSame(1, $datalayer['isearchresults']);

    // Create a second node to search for.
    $this->drupalGet('node/add/page');

    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains($this->t('@type @title has been created.', [
      '@type' => 'Basic page',
      '@title' => $edit['title[0][value]'],
    ]));

    // Index the node or it cannot found.
    $this->cronRun();
    $this->drupalGet('search/node');

    $this->submitForm($search, 'Search');

    // Check 2 results was found.
    $this->asertSame(2, $datalayer['isearchresults']);
  }

}
