<?php

declare(strict_types=1);

namespace Drupal\eulerian\Plugin\views\display_extender;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides interface to manage search data sent to Eulerian.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *     id = "eulerian",
 *     title = @Translation("Eulerian"),
 *     help = @Translation("Send data to Eulerian when search happens."),
 *     no_ui = FALSE
 * )
 */
class Eulerian extends DisplayExtenderPluginBase {

  /**
   * The facets manager service. If present.
   *
   * @var \Drupal\facets\FacetManager\DefaultFacetManager|null
   */
  protected ?DefaultFacetManager $facetsManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ContainerFactoryPluginInterface {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->facetsManager = $container->get('facets.manager', ContainerInterface::NULL_ON_INVALID_REFERENCE);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['enabled'] = ['default' => FALSE];
    $options['keys'] = ['default' => ''];
    $options['facets'] = ['default' => []];
    $options['concat_separator'] = ['default' => ','];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') != 'eulerian') {
      return;
    }
    $form['#title'] .= $this->t('Eulerian');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send search data to Eulerian'),
      '#default_value' => $this->options['enabled'],
    ];

    $form['search'] = [
      '#type' => 'details',
      '#title' => $this->t('Search keys'),
      '#open' => TRUE,
    ];

    $form['search']['keys'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GET parameters'),
      '#description' => $this->t('The GET parameters used to detect Eulerian search keys. Enter a comma-separated list, order matters.'),
      '#default_value' => $this->options['keys'],
    ];

    // Facets support.
    $query = $this->view->getQuery();
    if ($query instanceof SearchApiQuery && $this->facetsManager != NULL) {
      $facet_source = 'search_api:' . str_replace(':', '__', $query->getSearchApiQuery()->getSearchId());
      $facets = $this->facetsManager->getFacetsByFacetSourceId($facet_source);
      $facets_options = [];
      $facets_default_value = [];
      foreach ($facets as $facet) {
        $facet_id = $facet->id();
        $facets_options[$facet_id] = $facet->label();
        if (in_array($facet_id, $this->options['facets'], TRUE)) {
          $facets_default_value[] = $facet_id;
        }
      }

      $form['facets'] = [
        '#type' => 'details',
        '#title' => $this->t('Facets'),
        '#open' => TRUE,
      ];

      $form['facets']['facets'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Facets'),
        '#description' => $this->t('Use these facets as search keys, the active item is appended after
the facet ID. If a facet has multiple active items, its will be concatenated.'),
        '#default_value' => $facets_default_value,
        '#options' => $facets_options,
      ];
    }

    $form['concat_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Concatenation separator'),
      '#description' => $this->t('Concatenation separator when using multiple active elements.'),
      '#default_value' => $this->options['concat_separator'],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') != 'eulerian') {
      return;
    }

    /** @var array $form_state_values */
    $form_state_values = $form_state->cleanValues()->getValues();
    foreach ($form_state_values as $option => $value) {
      $this->options[$option] = $value;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function optionsSummary(&$categories, &$options): void {
    $options['eulerian'] = [
      'category' => 'other',
      'title' => $this->t('Eulerian'),
      'desc' => $this->t('Send data to Eulerian when search happens.'),
      'value' => $this->options['enabled'] ? $this->t('Yes') : $this->t('No'),
    ];
  }

}
