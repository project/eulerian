<?php

declare(strict_types=1);

namespace Drupal\eulerian\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eulerian\EulerianInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Eulerian module settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * List of strings in tokens with personal identifying information not
   * allowed for privacy reasons. See section 8.1 of the Google Analytics
   * terms of use for more detailed information.
   *
   * This list can never ever be complete. For this reason it tries to use a
   * regex and may kill a few other valid tokens, but it's the only way to
   * protect users as much as possible from admins with illegal ideas.
   *
   * User tokens are not prefixed with colon to catch 'current-user' and
   * 'user'.
   *
   * @var array
   */
  const TOKEN_FORBIDDEN_LIST = [
    ':account-name]',
    ':author]',
    ':author:edit-url]',
    ':author:url]',
    ':author:path]',
    ':current-user]',
    ':current-user:original]',
    ':display-name]',
    ':fid]',
    ':mail]',
    'user:name]',
    'author:name]',
    'owner:name]',
    ':uid]',
    ':one-time-login-url]',
    ':owner]',
    ':owner:cancel-url]',
    ':owner:edit-url]',
    ':owner:url]',
    ':owner:path]',
    'user:cancel-url]',
    'user:edit-url]',
    'user:url]',
    'user:path]',
    'user:picture]',
    // addressfield_tokens.module.
    ':first-name]',
    ':last-name]',
    ':name-line]',
    ':mc-address]',
    ':thoroughfare]',
    ':premise]',
    // realname.module.
    ':name-raw]',
    // token.module.
    ':ip-address]',
  ];

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ContainerInjectionInterface {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'eulerian_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['eulerian.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    // Visibility configurations.
    $form['visibility'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Visibility'),
      '#tree' => TRUE,
    ];
    $this->buildFormVisibility($form['visibility'], $form_state);

    // Tracking configurations.
    $form['tracking'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Tracking'),
      '#tree' => TRUE,
    ];
    $this->buildFormTracking($form['tracking'], $form_state);

    // Advanced feature configurations.
    $form['advanced'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Advanced settings'),
      '#tree' => TRUE,
    ];
    $this->buildFormAdvanced($form['advanced'], $form_state);

    // Custom parameters configurations.
    $form['custom_parameters'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Custom parameters'),
      '#description' => $this->t(
        'You can add Eulerian\'s <a href=":url">Custom Parameters</a> here.
These will be added to every page that Eulerian tracking code appears on.
Keep the names and values as short as possible and expect long values to get trimmed.
You may use tokens in custom parameter values.
Global and user tokens are always available; on node pages, node tokens are also available.',
        [':url' => 'https://eulerian.wiki/doku.php?id=en:offers:other:analytics:configure:custom_parameters']
      ),
      '#tree' => TRUE,
    ];
    $this->buildFormCustomParameters($form['custom_parameters'], $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $this->validateFormCustomParameters($form['custom_parameters'], $form_state);
    $this->validateFormVisibility($form['visibility'], $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('eulerian.settings');

    $config
      ->set('clean_string', $form_state->getValue(['advanced', 'clean_string']))
      ->set('custom.parameters', $form_state->getValue('custom_parameters'))
      ->set('track.domain', $form_state->getValue(['tracking', 'domain']))
      ->set('status_codes_disabled', array_values(
        array_filter($form_state->getValue(
          ['tracking', 'status_codes', 'status_codes_disabled']
          )
        )))
      ->set('track.colorbox', (bool) $form_state->getValue(
        ['tracking', 'colorbox']
      ))
      ->set('track.site_search', (bool) $form_state->getValue(
        ['tracking', 'search', 'site_search']
      ))
      ->set('track.userid', (bool) $form_state->getValue(['tracking', 'userid']))
      ->set('visibility.request_path_mode', $form_state->getValue(
        ['visibility', 'request_path_mode']
      ))
      ->set('visibility.request_path_pages', $form_state->getValue(
        ['visibility', 'request_path_pages']
      ));

    if ($form_state->hasValue(['advanced', 'translation_set'])) {
      $config->set('translation_set', (bool) $form_state->getValue(
        ['advanced', 'translation_set']
      ));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Build form advanced elements.
   *
   * @param array $elements
   *   An associative array
   *   containing the structure of the advanced form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function buildFormAdvanced(array &$elements, FormStateInterface $form_state): void {
    $config = $this->config('eulerian.settings');

    $elements['clean_string'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clean parameters before to send them'),
      '#description' => $this->t(
        'Eulerian recommends a <a href=":url">specific format</a> for the parameters to be collected.
This enables parameter to be formatted before to push it to Eulerian.',
        [':url' => 'https://eulerian.wiki/doku.php?id=en:setup:onsite_setup:tagging_general_rules#parameters_validation_rules']
      ),
      '#default_value' => $config->get('clean_string'),
    ];

    // Allow for tracking of the originating node when viewing translation sets.
    if ($this->moduleHandler->moduleExists('content_translation')) {
      $elements['translation_set'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Track translation sets as one unit'),
        '#description' => $this->t('When a node is part of a translation set, record statistics
for the originating node instead. This allows for a translation set to be treated as a single unit.'),
        '#default_value' => $config->get('translation_set'),
      ];
    }
  }

  /**
   * Build form custom parameters elements.
   *
   * @param array $elements
   *   An associative array
   *   containing the structure of the custom parameters form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function buildFormCustomParameters(array &$elements, FormStateInterface $form_state): void {
    $config = $this->config('eulerian.settings');

    $elements['slots'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Name')],
        ['data' => $this->t('Value')],
        ['data' => $this->t('Actions')],
      ],
      '#prefix' => '<div id="custom-parameters-slots-wrapper">',
      '#suffix' => '</div>',
    ];

    $customParams = $config->get('custom.parameters') ?: [];
    $customParamsNb = $form_state->get('nb_custom_parameters');

    if ($customParamsNb === NULL) {
      $customParamsNb = count($customParams) ?: 1;
      $form_state->set('nb_custom_parameters', $customParamsNb);
    }

    for ($i = 0; $i < $customParamsNb; $i++) {
      $element = [];
      $element['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Custom variable name #@slot', ['@slot' => $i]),
        '#title_display' => 'invisible',
        '#description' => $this->t('The custom variable name.'),
        '#default_value' => $customParams[$i]['name'] ?? '',
        '#maxlength' => 100,
        '#size' => 20,
      ];
      $element['value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Custom variable value #@slot', ['@slot' => $i]),
        '#title_display' => 'invisible',
        '#description' => $this->t('The custom parameter value.'),
        '#default_value' => $customParams[$i]['value'] ?? '',
        '#maxlength' => 255,
        '#element_validate' => [[static::class, 'tokenElementValidate']],
      ];
      if ($this->moduleHandler->moduleExists('token')) {
        $element['value']['#element_validate'][] = 'token_element_validate';
        $element['value']['#token_types'] = ['node'];
      }

      $element['actions'] = [
        '#type' => 'actions',
      ];
      $element['actions']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeCustomParametersSlot'],
        '#name' => 'custom-parameters-remove-' . $i,
        '#ajax' => [
          'callback' => '::ajaxCallbackCustomParametersSlot',
          'wrapper' => 'custom-parameters-slots-wrapper',
        ],
      ];

      $elements['slots'][$i] = $element;
    }

    $elements['actions'] = [
      '#type' => 'actions',
    ];
    $elements['actions']['add_slot'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add slot'),
      '#submit' => ['::addCustomParametersSlot'],
      '#ajax' => [
        'callback' => '::ajaxCallbackCustomParametersSlot',
        'wrapper' => 'custom-parameters-slots-wrapper',
      ],
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $elements['token_tree'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['node'],
      ];
    }
  }

  /**
   * Build form tracking elements.
   *
   * @param array $elements
   *   An associative array
   *   containing the structure of the tracking form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function buildFormTracking(array &$elements, FormStateInterface $form_state): void {
    $config = $this->config('eulerian.settings');

    $elements['linktracking'] = [
      '#type' => 'item',
      '#description' => $this->t('The informations transmitted during click events are automatically
collected on the link from its attributes prefixed by ":prefix".', [':prefix' => EulerianInterface::ATTR_PREFIX]),
    ];

    $elements['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $config->get('track.domain'),
      '#required' => TRUE,
    ];
    $elements['status_codes'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Status Codes'),
    ];
    $elements['status_codes']['status_codes_disabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Prevent tracking of pages with given HTTP Status code:'),
      '#options' => [
        '404' => $this->t('404 - Not found'),
        '403' => $this->t('403 - Access denied'),
      ],
      '#default_value' => $config->get('status_codes_disabled'),
    ];

    $elements['userid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track User ID'),
      '#default_value' => $config->get('track.userid'),
      '#description' => $this->t('User ID enables the analysis of groups of sessions, across devices,
using a unique, persistent, and non-personally identifiable ID string representing a user.'),
    ];

    $colorboxDependencies = '<div class="admin-requirements">';
    $colorboxDependencies .= $this->t(
      'Requires: @module-list',
      [
        '@module-list' => ($this->moduleHandler->moduleExists('colorbox') ? $this->t(
          '@module (<span class="admin-enabled">enabled</span>)',
          ['@module' => 'Colorbox']
        ) : $this->t(
          '@module (<span class="admin-missing">disabled</span>)',
          ['@module' => 'Colorbox']
        )),
      ]
    );
    $colorboxDependencies .= '</div>';

    $elements['colorbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track content in colorbox modal dialogs'),
      '#description' => $this->t('Enable to track the content shown in colorbox modal windows.') . $colorboxDependencies,
      '#default_value' => $config->get('track.colorbox'),
      '#disabled' => !$this->moduleHandler->moduleExists('colorbox'),
    ];

    $elements['search'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Search'),
    ];

    $searchDependencies = '<div class="admin-requirements">';
    $searchDependencies .= $this->t(
      'Requires: @module-list',
      [
        '@module-list' => ($this->moduleHandler->moduleExists('search') ? $this->t(
          '@module (<span class="admin-enabled">enabled</span>)',
          ['@module' => 'Search']
        ) : $this->t(
          '@module (<span class="admin-missing">disabled</span>)',
          ['@module' => 'Search']
        )),
      ]
    );
    $searchDependencies .= '</div>';

    $elements['search']['site_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track internal search'),
      '#description' => $this->t('If checked, internal search keywords are tracked.') . $searchDependencies,
      '#default_value' => $config->get('track.site_search'),
      '#disabled' => ($this->moduleHandler->moduleExists('search') ? FALSE : TRUE),
    ];
  }

  /**
   * Build form visibility elements.
   *
   * @param array $elements
   *   An associative array
   *   containing the structure of the visibility form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function buildFormVisibility(array &$elements, FormStateInterface $form_state): void {
    $config = $this->config('eulerian.settings');

    $options = [
      EulerianInterface::TRACKING_REQUEST_MODE_ALL => $this->t('Every page except the listed pages'),
      EulerianInterface::TRACKING_REQUEST_MODE_LISTED => $this->t('The listed pages only'),
    ];
    $title = $this->t('Pages');
    $description = $this->t(
      "Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard.
Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
      [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]
    );

    if (
      $this->moduleHandler->moduleExists('php') &&
      $this->currentUser()->hasPermission('use php for eulerian tracking visibility')
    ) {
      $options[EulerianInterface::TRACKING_REQUEST_MODE_PHP] = $this->t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = $this->t('Pages or PHP code');
      $description .= ' ' . $this->t('If the PHP option is chosen, enter PHP code between %php.
Note that executing incorrect PHP code can break your Drupal site.', ['%php' => '<?php ?>']);
    }

    $elements['request_path_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $config->get('visibility.request_path_mode'),
    ];
    $elements['request_path_pages'] = [
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $config->get('visibility.request_path_pages'),
      '#rows' => 10,
      '#description' => $description,
      '#states' => [
        // Note: Form required marker is not visible as title is invisible.
        'required' => [
          ':input[name="visibility[request_path_mode]"]' => ['!value' => EulerianInterface::TRACKING_REQUEST_MODE_ALL],
        ],
      ],
    ];
  }

  /**
   * Validate form custom parameters elements.
   *
   * @param array $elements
   *   An associative array
   *   containing the structure of the custom parameters form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function validateFormCustomParameters(array &$elements, FormStateInterface $form_state): void {
    $customParams = [];
    foreach ($form_state->getValue(['custom_parameters', 'slots']) as $index => $slot) {
      $slot['name'] = trim($slot['name']);
      $slot['value'] = trim($slot['value']);

      // Validate empty names/values.
      if (empty($slot['name']) && !empty($slot['value'])) {
        $form_state->setErrorByName(
          'custom_parameters][slots][' . $index . '][name',
          $this->t(
            'The custom parameter @slot requires a <em>Name</em> if a <em>Value</em> has been provided.',
            ['@slot' => $index + 1]
          )
        );
      }
      elseif (!empty($slot['name']) && empty($slot['value'])) {
        $form_state->setErrorByName(
          'custom_parameters][slots][' . $index . '][value',
          $this->t(
            'The custom parameter @slot requires a <em>Value</em> if a <em>Name</em> has been provided.',
            ['@slot' => $index + 1]
          )
        );
      }
      elseif (!empty($slot['name']) && !empty($slot['value'])) {
        unset($slot['actions']);
        $customParams[] = $slot;
      }
    }
    $form_state->setValue('custom_parameters', $customParams);
  }

  /**
   * Validate form visibility elements.
   *
   * @param array $elements
   *   An associative array
   *   containing the structure of the visibility form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function validateFormVisibility(array &$elements, FormStateInterface $form_state): void {
    // Trim text values.
    $form_state->setValue(
      ['visibility', 'request_path_pages'],
      trim($form_state->getValue(['visibility', 'request_path_pages']))
    );

    // Verify that every path is prefixed with a slash, but don't check PHP
    // code snippets and do not check for slashes if no paths configured.
    if (
      $form_state->getValue(['visibility', 'request_path_mode']) !== EulerianInterface::TRACKING_REQUEST_MODE_PHP &&
      !empty($form_state->getValue(['visibility', 'request_path_pages']))
    ) {
      $pages = preg_split('#(\r\n?|\n)#', $form_state->getValue(
        ['visibility', 'request_path_pages']
      ));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName(
            'visibility][request_path_pages',
            $this->t('Path "@page" not prefixed with slash.', ['@page' => $page])
          );

          // Drupal forms show one error only.
          break;
        }
      }
    }
  }

  /**
   * Callback for custom parameters slots.
   *
   * @param array $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The custom parameter's slots render array.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function ajaxCallbackCustomParametersSlot(array &$form, FormStateInterface $form_state): array {
    return $form['custom_parameters']['slots'];
  }

  /**
   * Submit handler for the "Add slot" button.
   *
   * Increments the max counter and causes a form rebuild.
   *
   * @param array $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function addCustomParametersSlot(array &$form, FormStateInterface $form_state): void {
    $count = $form_state->get('nb_custom_parameters');
    $form_state->set('nb_custom_parameters', ++$count);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "Add slot" button.
   *
   * Decrements the max counter and causes a form rebuild.
   *
   * @param array $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function removeCustomParametersSlot(array &$form, FormStateInterface $form_state): void {
    $count = $form_state->get('nb_custom_parameters');

    if ($count > 0) {
      $trigger = $form_state->getTriggeringElement();

      // Get removed slot index.
      $lastSlotHierarchy = array_slice($trigger['#parents'], 0, -2);
      $slotsHierarchy = array_slice($lastSlotHierarchy, 0, -1);
      $index = end($lastSlotHierarchy);

      // Remove slot entry.
      $userInput = $form_state->getUserInput();
      $slots = NestedArray::getValue($userInput, $slotsHierarchy);
      unset($slots[$index]);
      sort($slots);
      NestedArray::setValue($userInput, $slotsHierarchy, $slots);
      $form_state->setUserInput($userInput);

      $form_state->set('nb_custom_parameters', --$count);
    }

    $form_state->setRebuild();
  }

  /**
   * Validate if string contains forbidden tokens not allowed by privacy rules.
   *
   * @param string $tokenString
   *   A string with one or more tokens to be validated.
   *
   * @return bool
   *   TRUE if blocklisted token has been found, otherwise FALSE.
   */
  protected static function containsForbiddenToken(string $tokenString): bool {
    return preg_match(
      '#' . implode('|', array_map('preg_quote', static::TOKEN_FORBIDDEN_LIST)) . '#i',
      $tokenString
    );
  }

  /**
   * Get an array of all forbidden tokens.
   *
   * @param array $value
   *   An array of token values.
   *
   * @return array
   *   A unique array of invalid tokens.
   */
  protected static function getForbiddenTokens(array $value): array {
    $invalidTokens = [];

    foreach ($value as $tokens) {
      if (array_filter($tokens, 'static::containsForbiddenToken')) {
        $invalidTokens = array_merge($invalidTokens, array_values($tokens));
      }
    }

    return array_unique($invalidTokens);
  }

  /**
   * Validate a form element that should have tokens in it.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function tokenElementValidate(array &$element, FormStateInterface $form_state): void {
    $value = $element['#value'] ?? $element['#default_value'];

    if (empty($value)) {
      return;
    }

    $tokens = \Drupal::token()->scan($value);
    $invalidTokens = static::getForbiddenTokens($tokens);
    if ($invalidTokens) {
      $form_state->setError($element, t(
        'The %element-title is using the following forbidden tokens with personal identifying information: @invalid-tokens.',
        [
          '%element-title' => $element['#title'],
          '@invalid-tokens' => implode(', ', $invalidTokens),
        ]
      ));
    }
  }

}
