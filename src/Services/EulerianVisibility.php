<?php

declare(strict_types=1);

namespace Drupal\eulerian\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\eulerian\EulerianInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Provides Eulerian visibility functions.
 */
class EulerianVisibility implements EulerianVisibilityInterface {

  /**
   * The alias manager that caches alias lookups based on the request.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected AliasManagerInterface $aliasManager;

  /**
   * The Eulerian settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * The patch matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected PathMatcherInterface $pathMatcher;

  /**
   * EulerianVisibility constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    AliasManagerInterface $alias_manager,
    CurrentPathStack $current_path,
    PathMatcherInterface $path_matcher
  ) {
    $this->aliasManager = $alias_manager;
    $this->config = $config_factory->get('eulerian.settings');
    $this->currentPath = $current_path;
    $this->moduleHandler = $module_handler;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.ElseExpression)
   */
  public function isEnabledOnCurrentPage(): bool {
    $mode = $this->config->get('visibility.request_path_mode');
    $pages = $this->config->get('visibility.request_path_pages');

    if (
      $mode === EulerianInterface::TRACKING_REQUEST_MODE_ALL &&
      empty($pages)
    ) {
      return TRUE;
    }
    elseif (
      $mode === EulerianInterface::TRACKING_REQUEST_MODE_PHP &&
      $this->moduleHandler->moduleExists('php')
    ) {
      return php_eval($pages);
    }

    static $pageMatch;

    // Cache visibility result if function is called more than once.
    if (!isset($pageMatch)) {
      // Match path if necessary.
      $pageMatch = FALSE;

      if (!empty($pages)) {
        // Convert path to lowercase. This allows comparison of the same path
        // with different case. Ex: /Page, /page, /PAGE.
        $pages = mb_strtolower($pages);

        // Compare the lowercase path alias (if any) and internal path.
        $path = $this->currentPath->getPath();
        $pathAlias = $this->aliasManager->getAliasByPath($path);
        if (empty($pathAlias)) {
          $pathAlias = mb_strtolower($path);
        }
        else {
          $pathAlias = mb_strtolower($pathAlias);
        }

        $pageMatch = $this->pathMatcher->matchPath($pathAlias, $pages) ||
          ($path !== $pathAlias && $this->pathMatcher->matchPath($path, $pages));
      }
    }

    return (
        $mode === EulerianInterface::TRACKING_REQUEST_MODE_ALL &&
        !$pageMatch
      ) || (
        $mode !== EulerianInterface::TRACKING_REQUEST_MODE_ALL &&
        $pageMatch
      );
  }

}
