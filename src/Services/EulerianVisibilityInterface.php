<?php

declare(strict_types=1);

namespace Drupal\eulerian\Services;

/**
 * Provides an Eulerian visibility interface.
 */
interface EulerianVisibilityInterface {

  /**
   * Tracking visibility check for current page.
   *
   * @return bool
   *   Returns TRUE if JS code should be added
   *   to the current page and otherwise FALSE.
   */
  public function isEnabledOnCurrentPage(): bool;

}
