<?php

declare(strict_types=1);

namespace Drupal\eulerian\Services;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PrivateKey;
use Drupal\Core\Site\Settings;

/**
 * Provides Eulerian helper functions.
 */
class EulerianHelper implements EulerianHelperInterface {

  /**
   * The current path.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected LanguageInterface $language;

  /**
   * The private key service.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected PrivateKey $privateKey;

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected TransliterationInterface $transliteration;

  /**
   * EulerianHelper constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\PrivateKey $private_key
   *   The private key service.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   The transliteration service.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    PrivateKey $private_key,
    TransliterationInterface $transliteration
  ) {
    $this->language = $language_manager->getCurrentLanguage();
    $this->privateKey = $private_key;
    $this->transliteration = $transliteration;
  }

  /**
   * {@inheritdoc}
   */
  public function cleanString(string $value): string {
    // Remove accented characters.
    $value = $this->transliteration->transliterate($value, $this->language->getId(), '');

    // Keep letters, numbers, underscore.
    $value = preg_replace('/[^\w]+/', '_', $value);

    // Remove separator duplicates.
    $value = preg_replace('/_+/', '_', $value);

    // Convert to lower case and remove separators at the beginning
    // and the ending string.
    return trim(mb_strtolower($value), '_');
  }

  /**
   * {@inheritdoc}
   */
  public function generateUserIdentifierHash(int $uid): string {
    return Crypt::hmacBase64($uid, $this->privateKey->get() . Settings::getHashSalt());
  }

}
