<?php

declare(strict_types=1);

namespace Drupal\eulerian\Services;

/**
 * Provides an Eulerian helper interface.
 */
interface EulerianHelperInterface {

  /**
   * Clean string to expected format.
   *
   * @param string $value
   *   The string to clean.
   *
   * @return string
   *   The formatted string.
   */
  public function cleanString(string $value): string;

  /**
   * Generate user id hash to implement USER_ID.
   *
   * The UID value should be a unique, persistent, and non-personally
   * identifiable string identifier that represents a user or signed-in
   * account across devices.
   *
   * @param int $uid
   *   User id.
   *
   * @return string
   *   User id hash.
   */
  public function generateUserIdentifierHash(int $uid): string;

}
