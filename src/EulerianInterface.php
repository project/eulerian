<?php

declare(strict_types=1);

namespace Drupal\eulerian;

/**
 * Provides an interface.
 */
interface EulerianInterface {

  /**
   * Define the element attribute prefix.
   *
   * @var string
   */
  const ATTR_PREFIX = 'data-eulerian-';

  /**
   * Tracking request mode.
   *
   * @var string
   */
  const TRACKING_REQUEST_MODE_ALL = 'all';
  const TRACKING_REQUEST_MODE_LISTED = 'listed';
  const TRACKING_REQUEST_MODE_PHP = 'php';

}
