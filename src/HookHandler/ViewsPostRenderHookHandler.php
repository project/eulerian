<?php

declare(strict_types=1);

namespace Drupal\eulerian\HookHandler;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Hook handler for the hook_views_post_render() hook.
 */
class ViewsPostRenderHookHandler implements ContainerInjectionInterface {

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentRequest;

  /**
   * The facets manager service. If present.
   *
   * @var \Drupal\facets\FacetManager\DefaultFacetManager|null
   */
  protected ?DefaultFacetManager $facetsManager;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\facets\FacetManager\DefaultFacetManager|null $facetsManager
   *   The facets manager service. If present.
   */
  public function __construct(
    RequestStack $requestStack,
    ?DefaultFacetManager $facetsManager
  ) {
    $this->currentRequest = $requestStack->getCurrentRequest() ?: new Request();
    $this->facetsManager = $facetsManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ContainerInjectionInterface {
    return new static(
      $container->get('request_stack'),
      $container->get('facets.manager', ContainerInterface::NULL_ON_INVALID_REFERENCE)
    );
  }

  /**
   * Prepare data for Eulerian according to configuration.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view object being processed.
   * @param array $output
   *   A structured content array representing the view output. The given array
   *   depends on the style plugin and can be either a render array or an array
   *   of render arrays.
   * @param \Drupal\views\Plugin\views\cache\CachePluginBase $cache
   *   The cache settings.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function process(ViewExecutable $view, array &$output, CachePluginBase $cache): void {
    $display = $view->getDisplay();
    $extenders = $display->getExtenders();
    if (!isset($extenders['eulerian'])) {
      // If the ID of the plugin is not in the list then do nothing.
      return;
    }
    /** @var \Drupal\eulerian\Plugin\views\display_extender\Eulerian $extender */
    $extender = $extenders['eulerian'];

    $options = $extender->options;
    if (!$options['enabled']) {
      // If the plugin is not enabled on this view then do nothing.
      return;
    }

    $exposed_input = $view->getExposedInput();
    if (empty($exposed_input)) {
      // No explicit search done.
      return;
    }

    $output['#attached']['drupalSettings']['eulerian']['datalayer'] = [
      'isearchengine' => $view->storage->label(),
      'isearchkeys' => array_merge(
        $this->getSearchKeys($options),
        $this->getSearchKeysFromFacets($options, $view),
      ),
      'isearchresults' => $view->total_rows ?? count($view->result),
    ];
  }

  /**
   * The search keys from GET parameters.
   *
   * @param array $options
   *   The extender options.
   *
   * @return array
   *   The search keys.
   *
   * @SuppressWarnings(PHPMD.ElseExpression)
   */
  protected function getSearchKeys(array $options): array {
    $getParameters = $this->currentRequest->query->all();

    $searchKeys = [];
    $optionKeys = array_map('trim', explode(',', $options['keys']));
    foreach ($optionKeys as $key) {
      if (empty($getParameters[$key])) {
        continue;
      }

      if (is_array($getParameters[$key])) {
        $searchKeys[$key] = implode($options['concat_separator'], $getParameters[$key]);
      }
      else {
        $searchKeys[$key] = $getParameters[$key];
      }
    }

    return $searchKeys;
  }

  /**
   * The search keys from active facets items.
   *
   * @param array $options
   *   The extender options.
   * @param \Drupal\views\ViewExecutable $view
   *   The view object being processed.
   *
   * @return array
   *   The search keys.
   */
  protected function getSearchKeysFromFacets(array $options, ViewExecutable $view): array {
    $searchKeys = [];

    if (!empty($options['facets'])) {
      $query = $view->getQuery();
      if ($query instanceof SearchApiQuery && $this->facetsManager !== NULL) {
        $facets = $this->facetsManager->getFacetsByFacetSourceId(
          'search_api:' . str_replace(':', '__', $query->getSearchApiQuery()->getSearchId())
        );
        foreach ($facets as $facet) {
          if (!in_array($facet->id(), $options['facets'], TRUE)) {
            continue;
          }

          $facetValues = $facet->getActiveItems();
          if (empty($facetValues)) {
            continue;
          }

          $searchKeys[$facet->id()] = implode($options['concat_separator'], $facetValues);
        }
      }
    }

    return $searchKeys;
  }

}
