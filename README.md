# Eulerian Analytics


## Description

Adds the [Eulerian](https://www.eulerian.com/) tracking system to your website.

The module allows you to add the following statistics features to your site:
* Single domain tracking
* Selectively track/exclude certain pages
* Custom variables support with tokens
* Site Search
* Modal dialog tracking ([Colorbox](https://drupal.org/project/colorbox))
* Access denied (403) and Page not found (404) tracking
* User ID tracking across devices
* Asynchronous tracking
* A data cleaning system (using Eulerian's recommendations)
* Eulerian Tag Manager

## Requirements

* PHP >=7.3
* An Eulerian website domain


## Installation

* Install and enable this module like any other Drupal 8, 9 or 10 module.


## Configuration

In the settings page (/admin/config/system/eulerian), enter your Eulerian
website domain.

All pages will now have the required JavaScript added to the HTML header.

You can confirm this by viewing the page source from your browser.


### Page specific tracking

The default is set to "Add to every page except the listed pages". By
default the following pages are listed for exclusion:
```
/admin
/admin/*
/batch
/node/add*
/node/*/*
/user/*/*
```

These defaults are changeable by the website administrator or any other
user with "Administer Eulerian" permission.

Like the blocks visibility settings in Drupal core, there is a choice for
"Add if the following PHP code returns TRUE." Sample PHP snippets that can be
used in this textarea can be found on the handbook page "Overview-approach to
block visibility" at https://drupal.org/node/64135.


## Datalayer management

The datalayer is sent in Javascript from `drupalSettings.eulerian.datalayer`.

Using the module configuration, you can initiate a certain number of values
​​using the basic tags offered by Eulerian.

When sending the datalayer to Eulerian, it expects an iterating pairwise array
of data. Example:
```
EA_push(["key","value","key2","value2"]).
```

In order to simplify the alteration of data, they are transmitted in
`drupalSettings.eulerian.datalayer` as an object.

### Naming identical properties

In some cases because Eulerian allows the same property to be passed multiple
times, an object declaration is problematic.
For example, as part of a search, you can pass a set of search filters using the
"isearchkey" and "isearchdata" properties.

To allow multiple filters to be entered in an associative mode, "super
properties" have been made available.

#### Multiple couple "isearchkey", "isearchdata":

_Eulerian example:_
```
EA_push(["isearchkey","key1","isearchdata","value1","isearchkey","key2",
"isearchdata","value2","isearchkey","key3","isearchdata"," value3"]);
```
_Module suggestion:_
````
drupalSettings.eulerian.datalayer = {
  isearchkeys: {
    key1: "value1",
    key2: "value2",
    key3: "value3"
  }
};
````

#### Mutiple products:

_Eulerian example:_
```
EA_push(["prdref","ref1","prdamount","amount1","prdquantity","quantity1",
"prdref","ref2","prdamount","amount2","prdquantity","quantity2","prdref","ref3",
"prdamount","amount3","prdquantity","quantity3"]);
```
_Module suggestion:_
````
drupalSettings.eulerian.datalayer = {
  products: [
    {
        ref: "ref1",
        amount: "amount1",
        quantity: "quantity1"
    },
    {
        ref: "ref2",
        amount: "amount2",
        quantity: "quantity2"
    },
    {
        ref: "ref3",
        amount: "amount3",
        quantity: "quantity3"
    }
  ]
};
```` 

#### Mutiple product references:
  _Eulerian example:_
```
EA_push(["prdref","ref1","prdref","ref2","prdref","ref3"]);
```
_Module suggestion:_
````
drupalSettings.eulerian.datalayer = {
  prdrefs: ["prdref1","prdref2","prdref3"]
};
````

### Action event type

The same problem can arise for `action` events type.
It is possible to define several action parameters or even several actions
in the same event.
In the same approach, you can use `actions` and `actionparams` "super
properties".

## Links tracking

In order not to spam your Eulerian analysis, this module does not collect
any click events by default only links and areas with attributes prefixed
with `data-eulerian-` will pass information to Eulerian.

The attribute `_data-eulerian-event_` is mandatory to know the event type
to call. Otherwise, no event will be sent by the module.

You can override the event name by specifying the `data-eulerian-event-name`
attribute.

The information transmitted during click events are automatically
collected on the link from its attributes prefixed by `data-eulerian-`.

Example :
```
<a href="/contact" data-eulerian-event="link"
data-eulerian-event-name="Contact link">
Contact us</a>
```
Is equivalent to do Eulerian call :
```
EA_push("link", ["Contact link"])
```

Eulerian automatically detects events if you add Eulerian class like
`__EA_download__`. See official documentation for
more information (links below).


## Eulerian official documentation

* Homepage (choose your language) : https://eulerian.wiki/doku.php?id=start
* Homepage (EN) : https://eulerian.wiki/doku.php?id=en:start
* Events (EN) : 
  https://eulerian.wiki/doku.php?id=en:modules:collect:onsite_collection:eaevents
* TarteAuCitron implementation (FR) : 
  https://eulerian.wiki/doku.php?id=fr:setup:cmp:tarteaucitron

## Maintainers

* Sébastien Brindle (S3b0uN3t) - https://www.drupal.org/user/2989133
